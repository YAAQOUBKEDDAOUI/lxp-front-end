import { ImageUpload } from './ImageUpload'
import { hot } from 'react-hot-loader'

export default hot(module)(ImageUpload)
